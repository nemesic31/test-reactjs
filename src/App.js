import React from 'react';
import logo from './logo.svg';
import './App.css';
import LandingPage from './container/LandingPage/LandingPage';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import HomePage from './container/HomePage/HomePage';
import ProfilePage from './container/ProfilePage/ProfilePage';




function App() {

  
  return (
    <Router>
    
    <div className="App container" >
      <Switch>
        <Route path="/" exact component={LandingPage}/>
        <Route path="/Home" component={HomePage} />
        <Route path="/Profile" component={ProfilePage}/>

      </Switch>
      
      
    </div>
    </Router>

      

  );
}

export default App;
