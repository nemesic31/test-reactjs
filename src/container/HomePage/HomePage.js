import React, { Component } from 'react'
import ProfilePage from '../ProfilePage/ProfilePage'
import {Link} from 'react-router-dom'
import Card from '@material-ui/core/Card';
import '../../App.css'
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import FullWidthTabs from './FullWidthTabs'


export default class HomePage extends Component {
    
    render() {
        console.log(this.props.location.state.username)
        return (
           
            <div >
                <div style={{display:'flex',alignItems:'center',justifyContent:'center',height:'100vh'}}>
                 <Card style={{width: '30%', height: '65vh'}}>
                 <CardActionArea>
                 <CardMedia><img src={require('./logo.jpeg')} style={{paddingTop: 10}}/></CardMedia>
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
          Champion: {this.props.location.state.username}
          </Typography>
          
          <Typography variant="body2" color="textSecondary" >
          description<p/>
          type: carry,mage,tank,assasin<p/>
          skill: mushroom
          </Typography>
        </CardContent>
      </CardActionArea>

                {/* <p>My Profile</p> */}
                {/* <img src={require('./logo.jpeg')} /> */}
                {/* <p>nickname: {this.props.location.state.username}</p> */}
                {/* <p>
                    name : Somchok Wachirawattanakun
                </p>
                <p>Age: 23</p> */}
                <Link to="/Profile">
            <button >
                 <p>Calculator</p>
            </button>
            </Link>
            
            </Card>
            

                </div>

            
                
              
                
            </div>
        )
    }
}
